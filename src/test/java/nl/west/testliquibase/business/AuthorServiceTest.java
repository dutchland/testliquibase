package nl.west.testliquibase.business;


import nl.west.testliquibase.persistence.entities.Author;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;


@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthorServiceTest {

  @Autowired
  private AuthorService authorService;

  @Test
  public void getAllAuthorsReturnDataFromDatabase() {
    List<Author> authors = authorService.findAll().collect(Collectors.toList());
    assertFalse(authors.isEmpty());
    assertEquals(5, authors.size());
  }
}