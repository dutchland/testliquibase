package nl.west.testliquibase.persistence.repositories;

import nl.west.testliquibase.persistence.entities.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Integer> {

}
