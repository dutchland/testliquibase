package nl.west.testliquibase.persistence.repositories;


import nl.west.testliquibase.persistence.entities.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AuthorRepository {
    private final CrudRepository<Author, Integer> repository;

    @Autowired
    public AuthorRepository(AuthorCrudRepository repository) {
        this.repository = repository;
    }

    public Iterable<Author> findAll() {
        System.out.println("In AuthorRepository");
        return this.repository.findAll();
    }

    public Optional<Author> findById(int authorId) {
        return this.repository.findById(authorId);
    }
}
