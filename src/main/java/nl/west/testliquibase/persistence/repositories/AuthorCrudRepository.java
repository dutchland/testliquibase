package nl.west.testliquibase.persistence.repositories;

import nl.west.testliquibase.persistence.entities.Author;
import org.springframework.data.repository.CrudRepository;

interface AuthorCrudRepository extends CrudRepository<Author, Integer> {}