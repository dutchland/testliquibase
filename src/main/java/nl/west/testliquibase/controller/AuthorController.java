package nl.west.testliquibase.controller;

import nl.west.testliquibase.AuthorNotFoundException;
import nl.west.testliquibase.business.AuthorService;
import nl.west.testliquibase.persistence.entities.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/author")
public class AuthorController {
    private final AuthorService authorService;

    @Autowired
    public AuthorController(AuthorService authorService){
        this.authorService = authorService;
    }

    @RequestMapping()
    public Collection<Author> findAll() {
        return this.authorService.findAll()
                .collect(Collectors.toList());
    }

    @RequestMapping("/{authorId}")
    public Author findAll(@PathVariable(value="authorId") int authorId) {

        return this.authorService.findById(authorId)
                .orElseThrow(() -> new AuthorNotFoundException("Author not found"));
    }
}