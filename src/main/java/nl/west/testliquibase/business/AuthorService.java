package nl.west.testliquibase.business;


import nl.west.testliquibase.persistence.entities.Author;
import nl.west.testliquibase.persistence.repositories.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Component
public class AuthorService {
  private final AuthorRepository authorRepository;

  @Autowired
  public AuthorService(AuthorRepository authorRepository) {
    this.authorRepository = authorRepository;
  }

  public Stream<Author> findAll() {
    System.out.println("In AuthorService");
    return StreamSupport.stream(authorRepository.findAll().spliterator(), true);
  }

  public Optional<Author> findById(int authorId) {
    return authorRepository.findById(authorId);
  }
}
