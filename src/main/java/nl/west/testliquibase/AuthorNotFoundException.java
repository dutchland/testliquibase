package nl.west.testliquibase;

public class AuthorNotFoundException extends IllegalArgumentException {
    public AuthorNotFoundException(String message) {
        super(message);
    }
}
